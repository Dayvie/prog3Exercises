from PIL import Image
import matplotlib.pyplot as plt
import matplotlib


if __name__ == '__main__':
    image = Image.open('camera.png')
    plt.imshow(image, cmap=plt.cm.gray)
