from __future__ import print_function
import numpy as np
from multiprocessing import cpu_count, Process
from pi_queue_master import TaskManager
import math

def monte_carlo_worker(q_in, q_out):
    while True:
        result = 0
        workload = q_in.get()
        for _ in range(0, workload):
            xgen = np.random.random()
            ygen = np.random.random()
            if xgen**2 + ygen**2 <= 1.0:
                result += 1
        q_out.put(result)
        q_in.task_done()

def __start_workers(m):
    job_queue, result_queue = m.get_job_queue(), m.get_result_queue()
    nr_of_processes = cpu_count()
    processes = [Process(target=monte_carlo_worker,
            args=(job_queue, result_queue))
        for i in range(nr_of_processes)]
    for p in processes:
        p.start()
    return nr_of_processes

if __name__ == '__main__':
    from sys import argv, exit
    if len(argv) < 3:
        print('usage:', argv[0], 'server_IP server_socket')
        exit(0)
    server_ip = argv[1]
    server_socket = int(argv[2])
    TaskManager.register('get_job_queue')
    TaskManager.register('get_result_queue')
    m = TaskManager(address=(server_ip, server_socket), authkey=b'secret')
    m.connect()
    print("connected")
    nr_of_processes = __start_workers(m)
    print(nr_of_processes, 'workers started')
