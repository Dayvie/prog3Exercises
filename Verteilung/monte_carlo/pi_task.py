from __future__ import print_function

from pi_queue_master import TaskManager
import random, math, time
import numpy as np


def monte_carlo_process(m, n, workload):
    job_queue, result_queue = m.get_job_queue(), m.get_result_queue()
    for i in range(0, n):
        job_queue.put(workload)
    job_queue.join()
    return calc_pi(result_queue, workload, n)

def calc_pi(q_in, workload, n):
    hits = 0
    workload = workload
    while not q_in.empty():
        hits += q_in.get()
    return ((hits / n)*4)/workload

if __name__ == '__main__':
    from sys import argv, exit
    if len(argv) != 5:
        print('usage:', argv[0], 'server_IP server_socket number_of_iterations workload_per_iteration')
        exit(0)
    server_ip = argv[1]
    server_socket = int(argv[2])
    TaskManager.register('get_job_queue')
    TaskManager.register('get_result_queue')
    m = TaskManager(address=(server_ip, server_socket), authkey=b'secret')
    m.connect()
    t1 = time.time()
    result = monte_carlo_process(m, int(argv[3]), int(argv[4]))
    t2 = time.time()
    print(' result: ', result)
    print(' time:   ', t2-t1, ' s\n')
