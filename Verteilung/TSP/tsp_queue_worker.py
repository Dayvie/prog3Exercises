from __future__ import print_function

from multiprocessing import cpu_count, Process
from tsp_queue_master import TaskManager
import math

def __distance(p1, p2):
    delta_x = p2[0] - p1[0]
    delta_y = p2[1] - p1[1]
    return math.sqrt(delta_x * delta_x + delta_y * delta_y)

def __path_length(path):
    total_len = 0.0
    for i in range(1, len(path)):
        total_len += __distance(path[i-1], path[i])
    return total_len

def __shortest_closed_path(path_completed, path_ahead,
        min_length=None, min_path=None):
    if len(path_ahead) == 1:
        # create a closed path by adding the starting point
        total_path = path_completed + path_ahead + (path_completed[0], )
        total_path_length = __path_length(total_path)
        if min_length == None or total_path_length < min_length:
            min_length, min_path = total_path_length, total_path  
    else:
        # continue recursion
        for i in range(len(path_ahead)):
            min_length, min_path = \
                __shortest_closed_path(path_completed + (path_ahead[i], ),
                                       path_ahead[:i] + path_ahead[i+1:],
                                       min_length, min_path)
    return min_length, min_path

def __shortest_closed_path_tuple_call(arg_tupel):
    return __shortest_closed_path(arg_tupel[0], arg_tupel[1])

def __worker_function(job_queue, result_queue):
    while True:
        task = job_queue.get()
        result = __shortest_closed_path_tuple_call(task)
        result_queue.put(result)
        job_queue.task_done()

def __start_workers(m):
    job_queue, result_queue = m.get_job_queue(), m.get_result_queue()
    nr_of_processes = cpu_count()
    processes = [Process(target = __worker_function,
            args = (job_queue, result_queue))
        for i in range(nr_of_processes)]
    for p in processes:
        p.start()
    return nr_of_processes

if __name__ == '__main__':
    from sys import argv, exit
    if len(argv) < 3:
        print('usage:', argv[0], 'server_IP server_socket')
        exit(0)
    server_ip = argv[1]
    server_socket = int(argv[2])
    TaskManager.register('get_job_queue')
    TaskManager.register('get_result_queue')
    m = TaskManager(address=(server_ip, server_socket), authkey=b'secret')
    m.connect()
    print("connected")
    nr_of_processes = __start_workers(m)
    print(nr_of_processes, 'workers started')