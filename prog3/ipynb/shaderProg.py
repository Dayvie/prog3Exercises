from __future__ import print_function
from __future__ import division

__License = \
"""
Copyright (c) 2010, Peter Roesch <Peter.Roesch@hs-augsburg.de>
All rights reserved.

This software is distributed WITHOUT ANY WARRANTY; without even
the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE. See LICENSE.TXT in a parent directory for more information.
"""

import sys
try:
    import OpenGL
    OpenGL.FORWARD_COMPATIBLE_ONLY = True
    import OpenGL.GL as gl
except:
    print('Error importing GL / shaders')
    sys.exit()

class ShaderProgram (object):
    """Manage GLSL programs."""
    def __init__(self):
        self.__shaderProgramID = gl.glCreateProgram()
        self.__checkOpenGLError()
        self.__programReady = False
        self.__isEnabled = False
        self.__shaderList = []
        
    def __checkOpenGLError(self):
        """Print OpenGL error message."""
        err = gl.glGetError()
        if (err != gl.GL_NO_ERROR):
            print('GLERROR: ', gluErrorString(err))
            sys.exit()

    def reset(self):
        """Disable and remove all shader programs"""
        for shaderID in self.__shaderList:
            gl.glDetachShader(self.__shaderProgramID, shaderID)
            gl.glDeleteShader(shaderID)
            self.__shaderList.remove(shaderID)
            self.__checkOpenGLError()
        gl.glDeleteProgram(self.__shaderProgramID)
        self.__checkOpenGLError()
        self.__shaderProgramID = gl.glCreateProgram()
        self.__checkOpenGLError()
        self.__programReady = False
        
    def addShaderFromString(self, shaderType, sourceString):
        shaderHandle = gl.glCreateShader(shaderType)
        self.__checkOpenGLError()
        gl.glShaderSource(shaderHandle, [sourceString])
        self.__checkOpenGLError()
        gl.glCompileShader(shaderHandle)
        success = gl.glGetShaderiv(shaderHandle,
            gl.GL_COMPILE_STATUS)
        if (not success):
            print(gl.glGetShaderInfoLog(shaderHandle))
            sys.exit()
        gl.glAttachShader(self.__shaderProgramID, shaderHandle)
        self.__checkOpenGLError()
        self.__shaderList.append(shaderHandle)
        
    def addShader(self, shaderType, fileName):
        """Read a shader program from a file.

        The program is load and compiled"""
      
        sourceString = open(fileName, 'r').read()
        self.addShaderFromString(shaderType, sourceString)
        

    def linkShaders(self):
        """Link compiled shader programs."""
        gl.glLinkProgram(self.__shaderProgramID)
        self.__checkOpenGLError()
        success = gl.glGetProgramiv(self.__shaderProgramID, gl.GL_LINK_STATUS)
        if (not success):
            print(gl.glGetProgramInfoLog(self.__shaderProgramID))
            sys.exit()
        else:
            self.__programReady = True
    
    def enable(self):
        """Activate shader programs."""
        if self.__programReady:
            gl.glUseProgram(self.__shaderProgramID)
            self.__isEnabled = True
            self.__checkOpenGLError()
        else:
            print("Shaders not compiled/linked properly, enable() failed")

    def disable(self):
        """De-activate shader programs."""
        gl.glUseProgram(0)
        self.__isEnabled = False
        self.__checkOpenGLError()

    def indexOfUniformVariable(self, variableName, doWarn = True):
        """Find the index of a uniform variable."""
        if not self.__programReady:
            print("\nShaders not compiled/linked properly")
            result = -1
        else:
            result = gl.glGetUniformLocation(self.__shaderProgramID, \
                str.encode(variableName))
            self.__checkOpenGLError()
        if doWarn and result < 0:
            print('Warning: Variable "%s" not known to the shader' 
                % (variableName))
        return result

    def indexOfVertexAttribute(self, attributeName, doWarn = True):
        """Find the index of an attribute variable."""
        if not self.__programReady:
            print("\nShaders not compiled/linked properly")
            result = -1
        else:
            result = gl.glGetAttribLocation(self.__shaderProgramID, \
                str.encode(attributeName))
            self.__checkOpenGLError()
        if doWarn and result < 0:
            print('Warning: Attribute "%s" not known to the shader' 
                % (attributeName))
        return result

    def programParameter(self, id, val):
        """Set program parameter"""
        if not self.__programReady:
            print("\nShaders not compiled/linked properly")
            result = -1
        else:
            result = gl.glGetProgrami(self.__shaderProgramID, id, val)
            self.__checkOpenGLError()

    def validate(self):
        """Validate program"""
        gl.glValidateProgram(self.__shaderProgramID)
        print(gl.glGetProgramInfoLog(self.__shaderProgramID))
    
    def isEnabled(self):
        return self.__isEnabled

# test routine
if __name__ == '__main__':
    from OpenGL.GLUT import *
    glutInit(sys.argv)
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA)
    glutInitWindowSize(100, 100)
    glutCreateWindow(b"shaderProg Test")
    Sp = ShaderProgram()
    Sp.addShader(gl.GL_VERTEX_SHADER, "../simple/simpleInteraction.vert")
    Sp.addShader(gl.GL_FRAGMENT_SHADER, "../simple/simple.frag")
    Sp.linkShaders()
    Sp.validate()
    print("Index of variable transMatrix: ", 
        Sp.indexOfUniformVariable("transMatrix"))
    Sp.enable()
    print("Index of attribute inPosition: ", 
        Sp.indexOfVertexAttribute("inPosition"))
    Sp.disable()
    Sp.reset()
    print('OK')
