#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <unistd.h>
#include <pthread.h>

/* 
 * Parallel Monte Carlo simulation using posix threads.
 *
 * (c) 2008 Peter Roesch 
 */


/*
 * typdedef of calculation struct
 */
typedef struct{
	unsigned long dimension, nrOfHits, nrOfStepsDone, nrOfStepsToDo;
	pthread_mutex_t mutexCalc;
	unsigned int threadCounter, nrOfThreads;
} CalcStruct;

/*
* prototypes
*/
double unitSphereVolumeAccurate( unsigned dim );
double unitSphereVolumeMonteCarloMulti( unsigned dim
		, unsigned long stepNum, unsigned int threadNum );

int main(int argc, char **argv){
	if (argc != 4){
		printf("usage: %s nrOfdimensions nrOfMillionSteps nrOfThreads\n"
				, argv[0]);
		exit(0);
	}
	/*
	 * determine number of cores 
	 * taken from itk itkMultiThreader.cxx
	 */
	unsigned coreNum=1;
#ifdef _SC_NPROCESSORS_ONLN
    coreNum = sysconf( _SC_NPROCESSORS_ONLN );
#elif defined(_SC_NPROC_ONLN)
    coreNum = sysconf( _SC_NPROC_ONLN );
#endif
	printf("Number of cores: %d\n", coreNum);

	unsigned int dim = atoi( argv[1] );
	unsigned long stepNum = atol( argv[2] )*1000000L;
	unsigned int threadNum = atoi( argv[3] );
	double accVol = unitSphereVolumeAccurate( dim );
	printf("accurate value:    %g\n", accVol);
	double mcVol = unitSphereVolumeMonteCarloMulti( dim, stepNum, threadNum);
	printf("monte carlo value: %g\n", mcVol);
}	

/*
*	 MONTE_CARLO_APPROXIMATION
*
*	 random number generator taken from numerical recpies, ed 2
*/
#define MBIG 1000000000
#define MSEED 161803398
#define MZ 0
#define FAC (1.0/MBIG)

double ran3(long *idum, int *inext, int *inextp, long *ma, int *iff)
{
	long mj,mk;
	int i,ii,k;

	if (*idum < 0 || (*iff) == 0) {
		(*iff)=1;
		mj=labs(MSEED-labs(*idum));
		mj %= MBIG;
		ma[55]=mj;
		mk=1;
		for (i=1;i<=54;i++) {
			ii=(21*i) % 55;
			ma[ii]=mk;
			mk=mj-mk;
			if (mk < MZ) mk += MBIG;
			mj=ma[ii];
		}
		for (k=1;k<=4;k++)
			for (i=1;i<=55;i++) {
				ma[i] -= ma[1+(i+30) % 55];
				if (ma[i] < MZ) ma[i] += MBIG;
			}
		(*inext)=0;
		(*inextp)=31;
		*idum=1;
	}
	if (++(*inext) == 56) (*inext)=1;
	if (++(*inextp) == 56) (*inextp)=1;
	mj=ma[(*inext)]-ma[(*inextp)];
	if (mj < MZ) mj += MBIG;
	ma[(*inext)]=mj;
	return mj*FAC;
}
#undef MBIG
#undef MSEED
#undef MZ
#undef FAC

/*
 * function that performs simulation
 */
void *monteCarloSimulator( void *v){
	CalcStruct *calcStructPtr = (CalcStruct *) v;
	unsigned long i, stepNum, hits;
	/*
	 * nr of local  thread
	 */
	unsigned int threadID=0;
	/*
	 * variables for ran3
	 */
	long idnum = 33814579L;
	int inext,inextp;
	long ma[56];
	int iff=0;
	/*
	 * create unique thread ID
	 */
	pthread_mutex_lock(&(calcStructPtr->mutexCalc));
	threadID=calcStructPtr->threadCounter;
	calcStructPtr->threadCounter++;
	pthread_mutex_unlock(&(calcStructPtr->mutexCalc));
	/*
	 * make sure each thread generates different random numbers
	 */
	idnum+=threadID;
	/*
	 * calculation loop
	 */
	stepNum=calcStructPtr->nrOfStepsToDo/calcStructPtr->nrOfThreads;
	hits=0;
	for(i=0; i<stepNum; i++){
		double dist=0.0, r=0.0;
		unsigned long d=0;
		for (d=0; d<calcStructPtr->dimension; d++){
			r = 2.0*ran3(&idnum, &inext, &inextp, ma, &iff)-1.0;
			dist+=r*r;
		}
		if (dist <= 1.0)
			hits++;
	}
	/*
	 * store result
	 */
	pthread_mutex_lock(&(calcStructPtr->mutexCalc));
	calcStructPtr->nrOfHits+=hits;
	calcStructPtr->nrOfStepsDone+=stepNum;
	pthread_mutex_unlock(&(calcStructPtr->mutexCalc));

	pthread_exit((void*) 0);
}


double unitSphereVolumeMonteCarloMulti( unsigned int dim
		, unsigned long stepNum, unsigned int threadNum ){
	/*
	 * variables needed for threading
	 */
	pthread_attr_t attr;
	pthread_t *callThd;
	int threadCtr, rc;
	void *status;
	/*
	 * initialise calcStruct
	 */
	CalcStruct calcStruct;
	calcStruct.dimension = dim;
	calcStruct.nrOfStepsToDo = stepNum;
	calcStruct.nrOfHits=calcStruct.nrOfStepsDone=0L;
	calcStruct.nrOfThreads=threadNum;
	calcStruct.threadCounter=0;
	/*
	 * create threads
	 */
	pthread_mutex_init(&(calcStruct.mutexCalc), NULL);	
	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);
	callThd = (pthread_t *)(malloc(threadNum*sizeof(pthread_t)));
	for (threadCtr=0; threadCtr<threadNum; threadCtr++){
		rc=pthread_create( &callThd[threadCtr], &attr
				, monteCarloSimulator, (void *)(&calcStruct));
		if( rc!=0 ){
			printf("ERROR; return code from pthread_create() is %d\n", rc);
		 	exit(-1);
		}
	}
	pthread_attr_destroy(&attr);
	/*
	 * wait until work is done
	 */
	for (threadCtr=0; threadCtr<threadNum; threadCtr++){
		rc=pthread_join( callThd[threadCtr], &status);
		if( rc!=0 ){
			printf("ERROR; return code from pthread_join() is %d\n", rc);
		 	exit(-1);
		}
	}
	/*
	 * clean up
	 */
	free(callThd);
	pthread_mutex_destroy(&(calcStruct.mutexCalc));
	/*
	 * calculate and return result
	 */
	return (double)calcStruct.nrOfHits 
			/ (double)calcStruct.nrOfStepsDone * pow( 2.0, (double) dim);
}
/*
* ACCURATE_VALUES
*
* ln of gamma function as taken from numerical recipies, ed. 2
*/
double gammln(double xx)
{
	double x,y,tmp,ser;
	static double cof[6]={76.18009172947146,-86.50532032941677,
		24.01409824083091,-1.231739572450155,
		0.1208650973866179e-2,-0.5395239384953e-5};
	int j;

	y=x=xx;
	tmp=x+5.5;
	tmp -= (x+0.5)*log(tmp);
	ser=1.000000000190015;
	for (j=0;j<=5;j++) ser += cof[j]/++y;
	return -tmp+log(2.5066282746310005*ser/x);
}
/*
* volume of n-dimensional unit sphere, source:
* http://en.wikipedia.org/wiki/Unit_sphere
*/
double unitSphereVolumeAccurate( unsigned dim ){
	double dim_2 = (double)dim/2.0;
	double v = 2.0*pow(M_PI, dim_2) / exp(gammln(dim_2)) / dim;
	return v;
}
