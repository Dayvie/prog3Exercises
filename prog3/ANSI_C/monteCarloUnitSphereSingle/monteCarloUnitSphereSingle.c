#include <stdio.h>
#include <stdlib.h>
#include <math.h>
/*
* prototypes
*/
double unitSphereVolumeAccurate( unsigned dim );
double unitSphereVolumeMonteCarlo( unsigned dim, unsigned long stepNum );

int main(int argc, char **argv){
	if (argc != 3){
		printf("usage: %s nrOfdimensions nrOfMillionSteps\n"
				, argv[0]);
		exit(0);
	}
	unsigned int dim = atoi( argv[1] );
	unsigned long stepNum = atol( argv[2] )*1000000L;
	double accVol = unitSphereVolumeAccurate( dim );
	printf("accurate value:    %g\n", accVol);
	double mcVol = unitSphereVolumeMonteCarlo( dim, stepNum);
	printf("monte carlo value: %g\n", mcVol);
}	

/*
*	 MONTE_CARLO_APPROXIMATION
*
*	 random number generator taken from numerical recpies, ed 2
*/
#define MBIG 1000000000
#define MSEED 161803398
#define MZ 0
#define FAC (1.0/MBIG)

double ran3(long *idum)
{
	static int inext,inextp;
	static long ma[56];
	static int iff=0;
	long mj,mk;
	int i,ii,k;

	if (*idum < 0 || iff == 0) {
		iff=1;
		mj=labs(MSEED-labs(*idum));
		mj %= MBIG;
		ma[55]=mj;
		mk=1;
		for (i=1;i<=54;i++) {
			ii=(21*i) % 55;
			ma[ii]=mk;
			mk=mj-mk;
			if (mk < MZ) mk += MBIG;
			mj=ma[ii];
		}
		for (k=1;k<=4;k++)
			for (i=1;i<=55;i++) {
				ma[i] -= ma[1+(i+30) % 55];
				if (ma[i] < MZ) ma[i] += MBIG;
			}
		inext=0;
		inextp=31;
		*idum=1;
	}
	if (++inext == 56) inext=1;
	if (++inextp == 56) inextp=1;
	mj=ma[inext]-ma[inextp];
	if (mj < MZ) mj += MBIG;
	ma[inext]=mj;
	return mj*FAC;
}
#undef MBIG
#undef MSEED
#undef MZ
#undef FAC

double unitSphereVolumeMonteCarlo( unsigned int dim, unsigned long stepNum ){
	unsigned long hits=0, i=0, d=0;
	long idnum = 33814579L;
	double dist, r;
	for (i=0; i<stepNum; i++){
		dist=0.0;
		for (d=0; d<dim; d++){
			r = 2.0*ran3(&idnum)-1.0;
			dist+=r*r;
		}
		if (dist <= 1.0)
			hits++;
	}
	return (double)hits / (double)stepNum * pow( 2.0, (double) dim);
}
/*
* ACCURATE_VALUES
*
* ln of gamma function as taken from numerical recipies, ed. 2
*/
double gammln(double xx)
{
	double x,y,tmp,ser;
	static double cof[6]={76.18009172947146,-86.50532032941677,
		24.01409824083091,-1.231739572450155,
		0.1208650973866179e-2,-0.5395239384953e-5};
	int j;

	y=x=xx;
	tmp=x+5.5;
	tmp -= (x+0.5)*log(tmp);
	ser=1.000000000190015;
	for (j=0;j<=5;j++) ser += cof[j]/++y;
	return -tmp+log(2.5066282746310005*ser/x);
}
/*
* volume of n-dimensional unit sphere, source:
* http://en.wikipedia.org/wiki/Unit_sphere
*/
double unitSphereVolumeAccurate( unsigned dim ){
	double dim_2 = (double)dim/2.0;
	double v = 2.0*pow(M_PI, dim_2) / exp(gammln(dim_2)) / dim;
	return v;
}
