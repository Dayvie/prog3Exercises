#!/usr/bin/env python

"""
galaxy_widget.py

This example is based on the helloglwidget example and has been
extended by Peter.Roesch@hs-augsburg.de

A simple OpenGL custom widget for Qt Designer.

Copyright (C) 2007 David Boddie <david@boddie.org.uk>
Copyright (C) 2005-2006 Trolltech ASA. All rights reserved.

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
"""

from PyQt4 import QtCore, QtGui, QtOpenGL
from OpenGL import GL
from OpenGL import GLU
import math

class GalaxyWidget(QtOpenGL.QGLWidget):

    """GalaxyWidget(QtOpenGL.QGLWidget)

    Provides a custom widget to display a set of spheres.
    Various properties and slots are defined so that the user can rotate
    the spheres, and signals are defined to enable other components to
    react to changes to its orientation.

    The user can set the following properties:
        light_position: Position of the point light source. The object
            needs to provide attributes 'x', 'y' and 'z' corresponding
            to the coordinates of the light position.
        camera_position: Position of the OpenGL virtual camera. The object
            needs to provide attributes 'x', 'y' and 'z' corresponding
            to the coordinates of the camera. The camera is oriented
            towards the origin of the coordinate system.
    After these properties are set, the render method can be called.
        The parameter of the render method is an iterable providing
        objects with attributes 'x', 'y', 'z', 'r' where the first
        three arguments specify the position of the sphere and 'r'
        refers to the radius.
    For animations, a method startAnimation is provided. This method takes
        two arguments. The first one is a reference to a generator which
        creates an iterable as described above. An animation example is
        give at the bottom of this file.

    Usage example:
        widget = GalaxyWidget()
        Point3D = collections.namedtuple('Point3D', ['x', 'y', 'z'])
        widget.light_position = Point3D(1e31, 1e31, 1e31)
        widget.camera_position = Point3D(0, 0, 1e30)
        Sphere3D = collections.namedtuple('Mass3D', ['x', 'y', 'z', 'r'])
        sphere_list = []
        for r in range(1, 6):
            r_l = r * 1e29
            m = Sphere3D(r_l, r_l, r_l, r_l/10)
            sphere_list.append(m)
        widget.show()
        widget.render(sphere_list)
    """

    # We define three signals that are used to indicate changes to the
    # rotation of the logo.
    xRotationChanged = QtCore.pyqtSignal(int)
    yRotationChanged = QtCore.pyqtSignal(int)
    zRotationChanged = QtCore.pyqtSignal(int)

    def __init__(self, parent=None):
        super(GalaxyWidget, self).__init__(parent)
        self.bodies = None
        self.camera_position = None
        self.light_position = None
        self.sphere = 0
        self.xRot = 0
        self.yRot = 0
        self.zRot = 0
        self.lastPos = QtCore.QPoint()
        self.trolltechGray = QtGui.QColor.fromCmykF(1, 1, 1, 0.0)
        self.setWindowTitle(self.tr("Galaxy"))
        # scaling factor to avoid problems with large distances
        # the maximum 32-bit Float number is 3.4028237e38
        self.fScale = 1e-15
        # timer for animation
        self.timer = QtCore.QTimer()

    # The rotation of the logo about the x-axis can be controlled using the
    # xRotation property, defined using the following getter and setter
    # methods.

    def render(self, bodies):
        self.bodies = bodies
        self.updateGL()

    def getXRotation(self):
        return self.xRot

    # The setXRotation() setter method is also a slot.
    @QtCore.pyqtSlot(int)
    def setXRotation(self, angle):
        angle = self.normalizeAngle(angle)
        if angle != self.xRot:
            self.xRot = angle
            self.xRotationChanged.emit(angle)
            self.updateGL()

    xRotation = QtCore.pyqtProperty(int, getXRotation, setXRotation)

    # The rotation of the logo about the y-axis can be controlled using the
    # yRotation property, defined using the following getter and setter
    # methods.

    def getYRotation(self):
        return self.yRot

    # The setYRotation() setter method is also a slot.
    @QtCore.pyqtSlot(int)
    def setYRotation(self, angle):
        angle = self.normalizeAngle(angle)
        if angle != self.yRot:
            self.yRot = angle
            self.yRotationChanged.emit(angle)
            self.updateGL()

    yRotation = QtCore.pyqtProperty(int, getYRotation, setYRotation)

    # The rotation of the logo about the z-axis can be controlled using the
    # zRotation property, defined using the following getter and setter
    # methods.

    def getZRotation(self):
        return self.zRot

    # The setZRotation() setter method is also a slot.
    @QtCore.pyqtSlot(int)
    def setZRotation(self, angle):
        angle = self.normalizeAngle(angle)
        if angle != self.zRot:
            self.zRot = angle
            self.zRotationChanged.emit(angle)
            self.updateGL()

    zRotation = QtCore.pyqtProperty(int, getZRotation, setZRotation)

    def minimumSizeHint(self):
        return QtCore.QSize(50, 50)

    def sizeHint(self):
        return QtCore.QSize(200, 200)

    def initializeGL(self):
        self.qglClearColor(self.trolltechGray.dark())
        self.sphere = self.makeSphere()
        GL.glShadeModel(GL.GL_SMOOTH)
        GL.glEnable(GL.GL_DEPTH_TEST)
        GL.glEnable(GL.GL_CULL_FACE)
        GL.glEnable(GL.GL_LIGHTING)
        # make sure normal vectors of scaled spheres are normalised
        GL.glEnable(GL.GL_NORMALIZE)
        GL.glEnable(GL.GL_LIGHT0)
        GL.glLightfv(GL.GL_LIGHT0, GL.GL_POSITION, [1, 1, 1, 0])
        GL.glLightfv(GL.GL_LIGHT0, GL.GL_AMBIENT, [1.0, 1.0, 1.0, 1.0])
        GL.glLightfv(GL.GL_LIGHT0, GL.GL_DIFFUSE, [1.0, 1.0, 1.0, 1.0])
        GL.glLightfv(GL.GL_LIGHT0, GL.GL_SPECULAR, [1.0, 1.0, 1.0, 1.0])
        GL.glMaterialfv(GL.GL_FRONT, GL.GL_AMBIENT, [0.2, .2, .2, 1])
        GL.glMaterialfv(GL.GL_FRONT, GL.GL_DIFFUSE, [0.7, 0.7, 0.7, 1])
        GL.glMaterialfv(GL.GL_FRONT, GL.GL_SPECULAR, [0.1, 0.1, 0.1, 1])
        GL.glMaterialf(GL.GL_FRONT, GL.GL_SHININESS, 20)

    def paintGL(self):
        GL.glClear(GL.GL_COLOR_BUFFER_BIT | GL.GL_DEPTH_BUFFER_BIT)
        GL.glLoadIdentity()
        GLU.gluLookAt(
            self.camera_position.x * self.fScale,
            self.camera_position.y * self.fScale,
            self.camera_position.z * self.fScale,
            0.0, 0.0, 0.0,
            0.0, 1.0, 0.0)
        GL.glLightfv(GL.GL_LIGHT0, GL.GL_POSITION,
            [self.light_position.x * self.fScale,
             self.light_position.y * self.fScale,
             self.light_position.z * self.fScale, 1])
        GL.glRotated(self.xRot / 16.0, 1.0, 0.0, 0.0)
        GL.glRotated(self.yRot / 16.0, 0.0, 1.0, 0.0)
        GL.glRotated(self.zRot / 16.0, 0.0, 0.0, 1.0)
        for body in self.bodies:
            GL.glPushMatrix()
            GL.glTranslated(
                body.x * self.fScale,
                body.y * self.fScale,
                body.z * self.fScale)
            GL.glScalef(
                body.r * self.fScale,
                body.r * self.fScale,
                body.r * self.fScale)
            GL.glCallList(self.sphere)
            GL.glPopMatrix()

    def resizeGL(self, width, height):
        def distance_from_origin(p):
            r2 = p.x * p.x + p.y * p.y + p.z * p.z
            return math.sqrt(r2) * self.fScale
        GL.glViewport(0, 0, width, height)
        GL.glMatrixMode(GL.GL_PROJECTION)
        GL.glLoadIdentity()
        world_diameter = 2 * distance_from_origin(self.camera_position)
        GLU.gluPerspective(60, float(width) / float(height),
           world_diameter / 1e4, world_diameter)
        GL.glMatrixMode(GL.GL_MODELVIEW)

    def mousePressEvent(self, event):
        self.lastPos = QtCore.QPoint(event.pos())

    def mouseMoveEvent(self, event):
        dx = event.x() - self.lastPos.x()
        dy = event.y() - self.lastPos.y()

        if event.buttons() & QtCore.Qt.LeftButton:
            self.setXRotation(self.xRot + 8 * dy)
            self.setYRotation(self.yRot + 8 * dx)
        elif event.buttons() & QtCore.Qt.RightButton:
            self.setXRotation(self.xRot + 8 * dy)
            self.setZRotation(self.zRot + 8 * dx)

        self.lastPos = QtCore.QPoint(event.pos())

    def makeSphere(self):
        genList = GL.glGenLists(1)
        GL.glNewList(genList, GL.GL_COMPILE)
        quadObj = GLU.gluNewQuadric()
        GLU.gluQuadricDrawStyle(quadObj, GLU.GLU_FILL)
        GLU.gluQuadricNormals (quadObj, GLU.GLU_SMOOTH)
        GLU.gluSphere(quadObj, 1, 16, 16);
        GL.glEndList()
        return genList

    def normalizeAngle(self, angle):
        while angle < 0:
            angle += 360 * 16
        while angle > 360 * 16:
            angle -= 360 * 16
        return angle

    def animationStep(self):
        self.bodies = next(self.bodyGenerator)
        self.updateGL()

    def startAnimation(self, bodyGenerator, fps = 25.0):
        self.bodyGenerator = bodyGenerator()
        # frames per second for GL update
        self.connect(self.timer, QtCore.SIGNAL('timeout()'), 
            self.animationStep)
        self.timer.setInterval(1000 / fps)
        self.timer.start()

if __name__ == "__main__":
    import sys
    import collections
    app = QtGui.QApplication(sys.argv)
    # create widget instance
    widget = GalaxyWidget()
    # named tuple (can be replaced by a class with attributes x, y, z)
    Point3D = collections.namedtuple('Point3D', ['x', 'y', 'z'])
    # set camera and light position. The large values have been made
    # possible by introducing the attribute fScale
    widget.light_position = Point3D(1e22, 1e22, 1e22)
    widget.camera_position = Point3D(0, 0, 3e20)
    # named tuple (can be replaced by a class with attributes x, y, z, r)
    Sphere3D = collections.namedtuple('Mass3D', ['x', 'y', 'z', 'r'])
    # set up initial sphere list
    sphere_list = []
    for r in range(1, 8):
        r_l = r * 1e19
        m = Sphere3D(r_l, r_l, r_l, r_l/10)
        sphere_list.append(m)
    # render initial list
    widget.render(sphere_list)
    # display widget
    widget.show()

    # Generator providing updated sphere lists
    # Here, no simulation is used but objects are moved on circles
    # around the origin for demonstration.
    def bodyGenerator():
        alpha = 0.0
        deltaAlpha = 0.01
        counter = 0
        while True:
            new_list = []
            for i, s in enumerate(sphere_list):
                j = len(sphere_list) - i
                sa = math.sin(alpha * j)
                ca = math.cos(alpha * j)
                # 2D rotation around the origin
                x =   s.x * ca - s.y * sa
                y =   s.x * sa + s.y * ca
                m = Sphere3D(x, y, s.z, s.r)
                new_list.append(m)
            # update rotation angle
            alpha += deltaAlpha
            counter += 1
            # return new position list
            yield new_list
    # start animation with 60 frames per second
    widget.startAnimation(bodyGenerator, fps=60)
    # pass control to Qt event loop
    sys.exit(app.exec_())
