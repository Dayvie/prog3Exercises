"""
@author David Yesil
"""
import unittest
from unittest import mock


class HimmelskoerperTest(unittest.TestCase):

    def setUp(self):
        self.p = Planet()

    def test_mass(self):
        self.assertGreater(self.p.mass, 0)
        self.p.update()
        self.assertGreater(self.p.mass, 0)

with mock.MagicMock() as Planet:
    """
    Teste die Masse des Himmelskoerpers
    """
    p = Planet()
    p.update()
    p.mass = 50013379001
    suite = unittest.TestLoader().loadTestsFromTestCase(
                                    HimmelskoerperTest)
    unittest.TextTestRunner(verbosity=1).run(suite)
