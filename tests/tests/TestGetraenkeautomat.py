"""
@author David Yesil
"""

import unittest
from automaten.meine_automaten import Getraenkeautomat, AutomatenZustaende


class GetraenkeautomatenTest(unittest.TestCase):

    def setUp(self):
        self.g = Getraenkeautomat()

    def test_first_state(self):
        self.assertEqual(self.g.zustand, AutomatenZustaende.ANFANG)

    def test_buy_state(self):
        self.g.eingabe("50Cent")
        self.assertEqual(self.g.zustand, AutomatenZustaende.AUSWAHL)

    def test_fail_state(self):
        self.g.eingabe("10Cent")
        self.assertEqual(self.g.zustand, AutomatenZustaende.FALSCHEMUENZE)

    def test_kosten_getraenk(self):
        self.assertEqual(self.g.kosten_getraenk, 50)

if __name__ == "__main":
    suite = unittest.TestLoader().loadTestsFromTestCase(GetraenkeautomatenTest)
    unittest.TextTestRunner(verbosity=1).run(suite)
