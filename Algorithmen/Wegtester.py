import math


    
def calc_dist(stadt1, stadt2):
        """
        kalkuliert die Distanz zwischen zwei Städten. Wandelt negative Beträge 
        vor der Berechnung in positive um
        """
        print(stadt1)
        print(stadt2)
        x = stadt2[0] - stadt1[0]
        y = stadt2[1] - stadt1[1]
        c_quadrat = y*y + x*x
        return math.sqrt(c_quadrat)

def calc_dists(weg):
    distanz = 0
    for i in range(0, 9):
        distanz += calc_dist(weg[i], weg[i+1])
    return distanz
        
    


if __name__ == "__main__":
    weg = [(0.010319427306382911, 0.8956251389386756),
     (0.07528757443413125, 0.07854082131763074),
     (0.4294574582950912, 0.4568408794115657),
     (0.6999898714299346, 0.42254500074835377),
     (0.748521134122647, 0.5437775417153159),
     (0.7571232013282426, 0.606435031856663),
     (0.9590226056623925, 0.581453646599427),
     (0.6005454852683483, 0.9295407203370832),
     (0.32346175150639334, 0.7291706487873425),
     (0.010319427306382911, 0.8956251389386756)]
    print(calc_dists(weg))
