# -*- coding: utf-8 -*-
"""
Created on Sun Oct 18 16:50:51 2015

@author: David
"""

import scipy as scp
import numpy as np
import matplotlib.pyplot as plt

global G
G = 6.67428e-11


class Planet(object):
    def __init__(self, name, initial_distance, mass, velocity, sun):
        self.plot_counter = 0
        self.sun = sun
        self.pos_list_x = []
        self.pos_list_y = []
        self.pos_vector = np.array([0, initial_distance])
        self.mass = mass
        self.name = name
        self.velocity_vector = np.array([-velocity, 0])
        self.time_old = 1

    def update(self, time_new):
        self.save_pos()
        self.apply_tangential_velocity()
        self.apply_grav(time_new)
        self.time_old = time_new

    @property
    def distance_sun(self):
        """
        :return: the distance between the sun and the planet
        """
        return np.linalg.norm(self.pos_vector - self.sun.pos)

    def dt(self, time_new):
        """
        :return: deltatime
        """
        return time_new - self.time_old

    def apply_tangential_velocity(self):
        self.pos_vector = self.velocity_vector + self.pos_vector

    def apply_grav(self, time_new):
        self.grav_acc = \
            -(((self.mass * self.sun.mass)/(self.distance_sun**3))* G * (self.pos_vector - sun.pos))/ self.mass
        self.pos_vector =\
            (self.pos_vector + self.dt(time_new) * self.velocity_vector + (1 / 2) * self.grav_acc * \
                                                                         self.dt(time_new) ** 2)
        self.velocity_vector += self.grav_acc * self.dt(time_new)

    def save_pos(self):
        self.pos_list_x.append(self.pos_vector[0])
        self.pos_list_y.append(self.pos_vector[1])

    def plot_positions(self):
        plt.plot(self.pos_list_x, self.pos_list_y)
        self.pos_list_x.clear()
        self.pos_list_y.clear()


class Sun(object):
    def __init__(self):
        self.pos = np.array([0, 0])
        self.mass = 1.989 * 10**30


def timeloop_planets(border, list_planets):
    time_new = 2
    for _ in range(0, border):
        for planet in list_planets:
            planet.save_pos()
            print(planet.pos_vector)
            planet.update(time_new)
        time_new += 1
    for planet in list_planets:
        plt.plot(planet.pos_list_x, planet.pos_list_y)
    sun_x = [sun.pos[0]]
    sun_y = [sun.pos[1]]
    plt.plot(sun_x, sun_y)
    plt.show()

if __name__ == "__main__":
    sun = Sun()
    earth = Planet("Erde", 149600000, 5.974 * 10 ** 24, 29.78, sun)
    list_planets = [earth]
    timeloop_planets(125, list_planets)
