import math
import functools

def calc_equation(x):
    """
    calculates the value for the given x
    """
    return math.cos(x)*math.e**(-x**2)

def map_equation(list_param):
    """
    calculates the y values of the equation with the x values in list_param
    :param list_param: the x values for the equation
    :return: the calculated y values
    """
    return list(map(calc_equation, list_param))

def equation_example():
    list_param = []
    for i in range(0, 8):
        list_param.append(i)
    print(map_equation(list_param))

def max_reduce(list_param):
    """
    returns the biggest value of a list
    """
    return functools.reduce(max, list_param)

def max(x, y):
    """
    compares two values and returns the bigger one
    """
    if(x>y):
        return x
    else:
        return y

if __name__ == "__main__":
    equation_example()
    list_param = [4, 100, 23, 2222222222, 34, 64646, 78, 123, 23, 567457, 345345345]
    print(max_reduce(list_param))