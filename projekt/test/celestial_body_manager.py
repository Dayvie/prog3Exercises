"""
Created on Sun Oct 18 16:50:51 2015

@author: David, Chris
"""


from abc import ABCMeta
import copy
import numpy as np


global G
G = 6.67428e-11


class CelestialBody(object):

    __metaclass__ = ABCMeta

    def __init__(self, name, initial_distance, mass, velocity, logic_manager):
        self.pos_vector = np.array([0, initial_distance, 0], dtype='f')
        self.mass = mass
        self.name = name
        self.velocity_vector = np.array([velocity, 0, 0], dtype='f')
        self.logic_manager = logic_manager

    def update(self, copy_list):
        self.apply_tangential_velocity()
        gravitational_vector = np.array([0, 0, 0], dtype='f')
        for celestial_body in copy_list:
            gravitational_vector = gravitational_vector + self.calculate_gravitational_vector(celestial_body)
        self.calculate_positional_vector_after_gravitational_vector(gravitational_vector)
        self.update_tangential_vector(gravitational_vector)
        print(self.name + "    " + str(self.pos_vector))

    def distance_celestial_body(self, celestial_body):
        """
        :return: the distance between two planets
        """
        return np.linalg.norm(self.pos_vector - celestial_body.pos_vector)

    def apply_tangential_velocity(self):
        self.pos_vector = self.velocity_vector + self.pos_vector

    def calculate_gravitational_vector(self, celestial_body):
        grav_acc = \
            -(((self.mass * celestial_body.mass)/(self.distance_celestial_body(celestial_body)**3))* G * 
              (self.pos_vector - celestial_body.pos_vector))/ self.mass
        return grav_acc
    
    def calculate_positional_vector_after_gravitational_vector(self, gravitational_vector):
        self.pos_vector = \
            (self.pos_vector + self.logic_manager.fps_interval * self.velocity_vector + 0.5 * gravitational_vector *\
                                                                         self.logic_manager.fps_interval ** 2)
        
    def update_tangential_vector(self, gravitational_vector):
        self.velocity_vector = gravitational_vector * self.logic_manager.fps_interval + self.velocity_vector


class Planet(CelestialBody):
    def __init__(self, name, initial_distance, mass, velocity, logic_manager):
        super().__init__(name, initial_distance, mass, velocity, logic_manager)


class CelestialBodyManager(object):
    def __init__(self):
        mercury = Planet("Merkur", 57910000000, 328.5 * 10**21, 47360, self)
        venus = Planet("Venus", 108200000000, 4.869 * 10 ** 24, 35020, self)
        earth = Planet("Erde", 149600000000, 5.974 * 10**24, 29780, self)
        mars = Planet("Mars", 227900000000, 3.301*10**23, 24130, self)
        jupyter = Planet("Jupiter", 778500000000, 1.898 * 10**27, 13070, self)
        saturn = Planet("Saturn", 1433000000000, 568.3 * 10 ** 24, 9690, self)
        uranus = Planet("Uranus", 2877000000000, 86.81 * 10**24, 6810, self)
        neptun = Planet("Neptun", 4498000000000, 102.4 * 10**24, 5430, self)
        sun = Planet("Sonne", 1, 1.989 * 10**30, 1, self )
        #list_bodies = [mercury, venus, earth, mars, jupyter, saturn, uranus, neptun]
        list_bodies = [earth, sun] #
        self.celestial_bodies = {}
        for celestial_body in list_bodies:
            self.celestial_bodies[celestial_body.name] = celestial_body
        self.fps_interval = 1000
                
    def update_all(self):
        copy_list = copy.deepcopy(list(self.celestial_bodies.values()))
        for current_celestial_body in copy_list:
            copied_celestial_body = copy.deepcopy(current_celestial_body)
            copied_celestial_body.update(copy_list)
            self.celestial_bodies[copied_celestial_body.name] = copied_celestial_body
