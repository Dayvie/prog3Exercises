# -*- coding: utf-8 -*-
"""
GUI Modul fuer die Planetensimulation

@author Christian Gubo, David Yesil
"""

from PyQt4 import QtCore, QtGui, uic
import collections, sys

class GalaxyGraphicalUserInterface (QtGui.QMainWindow):
    def __init__(self, logic_manager):
        super().__init__()
        self.logic_manager = logic_manager
        self.ui = uic.loadUi('galaxy.ui')
        self.widget = self.ui.galaxyWidget
        self.timer = QtCore.QTimer()
        self.connect(self.timer, QtCore.SIGNAL('timeout()'),
            self.render_celestial_bodies)
        self.connect(self.ui.actionExit, QtCore.SIGNAL('triggered()'),
            self.close)
        self.fps = 60
        self.timer.setInterval(1000 / self.fps)
        self.Point3D = collections.namedtuple('Point3D', ['x', 'y', 'z'])
        self.Sphere3D = collections.namedtuple('Mass3D', ['x', 'y', 'z', 'r'])
        self.widget.light_position = self.Point3D(1e24, 1e23, 1e23)
        self.widget.camera_position = self.Point3D(0, 0, 3e21)
        self.timer.start()
        self.ui.show()
        self.widget.show()

    def close(self):
        """
        @author David Yesil
        closes the GUI and messages the logic_manager to close itself
        :return:
        """
        self.logic_manager.closeFlag = True
        self.ui.close()
        
    def render_celestial_bodies(self):
        sphere_list = []
        celestial_bodies_snapshot = list(self.logic_manager.celestial_body_manager.celestial_bodies.values())[:]
        for celestial_body in celestial_bodies_snapshot:
            sphere_list.append(self.Sphere3D(celestial_body.pos_vector[0], celestial_body.pos_vector[1],
                                             celestial_body.pos_vector[2], 1e20))
        self.widget.render(sphere_list)
