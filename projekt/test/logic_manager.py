# -*- coding: utf-8 -*-
"""
@author: Christian Gubo, David Yesil
"""
from gui_manager import GalaxyGraphicalUserInterface
from celestial_body_manager import CelestialBodyManager
from PyQt4 import QtCore, QtGui, uic
import sys, time, threading


class LogicManager(object):

    def __init__(self, app):
        self.app = app
        self.celestial_body_manager = CelestialBodyManager()
        self.gui = GalaxyGraphicalUserInterface(self)
        self.closeFlag = False
        self.logic_thread = threading.Thread(target=self.update_loop)
        self.logic_thread.start()

    def update_loop(self):
        """
        @author David Yesil
        updates the logic until the window is closed
        """
        while not self.closeFlag:
            self.celestial_body_manager.update_all()
            time.sleep(2*2**-5)
        else:
            sys.exit(0)

if __name__ == "__main__":
    sys.path.append("widgets")
    app = QtGui.QApplication(sys.argv)
    logic_manager = LogicManager(app)
    sys.exit(app.exec_())

