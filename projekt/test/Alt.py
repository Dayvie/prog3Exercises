"""
Created on Sun Oct 18 16:50:51 2015

@author: David
"""

import math
import numpy as np
import matplotlib.pyplot as plt
from abc import ABCMeta, abstractmethod

global G
G = 6.67428e-11


class CelestialBody(object):

    __metaclass__ = ABCMeta

    def __init__(self, name, initial_distance, mass, velocity, time_of_circulation, sun):
        self.sun = sun
        self.pos_list_x = []
        self.pos_list_y = []
        self.pos_list_z = []
        self.pos_vector = np.array([0, initial_distance, 0], dtype='f')
        self.mass = mass
        self.name = name
        self.velocity_vector = np.array([velocity, 0, 0], dtype='f')
        self.time_old = 0
        self.time_of_circulation = time_of_circulation

    def update(self, time_new):
        print(self.pos_vector)
        self.save_pos()
        self.apply_tangential_velocity()
        self.apply_grav(time_new)
        self.update_velocity(time_new)
        self.time_old = time_new

    @property
    def distance_sun(self):
        """
        :return: the distance between the sun and the planet
        """
        return np.linalg.norm(self.pos_vector - self.sun.pos)

    def dt(self, time_new):
        """
        :return: deltatime
        """
        return time_new - self.time_old

    def apply_tangential_velocity(self):
        self.pos_vector = self.velocity_vector + self.pos_vector

    def apply_grav(self, time_new):
        self.grav_acc = \
            -(((self.mass * self.sun.mass)/(self.distance_sun**3))* G * (self.pos_vector - self.sun.pos))/ self.mass
        self.pos_vector = \
            (self.pos_vector + self.dt(time_new) * self.velocity_vector + 0.5 * self.grav_acc * \
                                                                         self.dt(time_new) ** 2) #apply gravity

    def update_velocity(self, time_new):
        self.velocity_vector = self.grav_acc * self.dt(time_new) + self.velocity_vector

    def save_pos(self):
        self.pos_list_x.append(self.pos_vector[0])
        self.pos_list_y.append(self.pos_vector[1])
        self.pos_list_z.append(self.pos_vector[2])


class Planet(CelestialBody):
    def __init__(self, name, initial_distance, mass, velocity, time_of_circulation, sun):
        super().__init__(name, initial_distance, mass, velocity, time_of_circulation, sun)


class Sun(object):
    def __init__(self):
        self.name = "Sonne"
        self.pos = np.array([0, 0, 0])
        self.mass = 1.989 * 10**30


class LogicManager(object):
    def __init__(self):
        self.sun = Sun()
        self.border = 24*360
        self.time_new = 2
        mercury = Planet("Merkur", 57910000000, 328.5 * 10**21, 47360, 88, self.sun)
        venus = Planet("Venus", 108200000000, 4.869 * 10 ** 24, 35020, 225, self.sun)
        earth = Planet("Erde", 149600000000, 5.974 * 10**24, 29780, 365, self.sun)
        mars = Planet("Mars", 227900000000, 3.301*10**23, 24130, 687, self.sun)
        jupyter = Planet("Jupiter", 778500000000, 1.898 * 10**27, 13070, 365*11+315, self.sun)
        saturn = Planet("Saturn", 1433000000000, 568.3 * 10 ** 24, 9690, 365*29+161, self.sun)
        uranus = Planet("Uranus", 2877000000000, 86.81 * 10**24, 6810, 365*84+4, self.sun)
        neptun = Planet("Neptun", 4498000000000, 102.4 * 10**24, 5430, 365*164+288, self.sun)
        #list_bodies = [mercury, venus, earth, mars, jupyter, saturn, uranus, neptun]
        list_bodies = [earth]
        self.celestial_bodies = {}
        for celestial_body in list_bodies:
            self.celestial_bodies[celestial_body.name] = celestial_body
        self.timeloop_planets(self.border)

    def timeloop_planets(self, border):
        for i in range(0, border):
            for celestial_body in self.celestial_bodies.values():
                celestial_body.save_pos()
                celestial_body.update(self.time_new*celestial_body.time_of_circulation)
            self.time_new += 10

if __name__ == "__main__":
    logic_manager = LogicManager()
