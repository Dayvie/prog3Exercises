# -*- coding: utf-8 -*-
# pylint: disable=W0141
"""
Created on Sat Oct 17 14:36:56 2015

@author: David Yesil
"""
from __future__ import print_function
import sys
import random
import inspect
import pickle
from abc import ABCMeta, abstractmethod


class VokabelTrainer(object):
    """
    Eine software zum Abfragen von Vokabeln
    """
    def __init__(self, textEdit, label_right, label_wrong):
        """
        initialisiert das Wörterbuch
        """
        self.label_right = label_right
        self.label_wrong = label_wrong
        self.train_flag = False
        self.textEdit = textEdit
        self.commands = {"hinz": self.hinzufuegen,
                         "train": self.train_gui,
                         "zrk": self.zuruecksetzen,
                         "aus": self.ergebnis_ausgabe,
                         "vok": self.gebe_vokabeln_aus,
                         "entf": self.loesche_wort,
                         "hilfe": self.gebe_befehle_aus,
                         "q": self.schliessen}
        self.lade_vokabeln()
        self.richtig = 0
        self.falsch = 0
        self.befehle = "Liste an Befehlen\n"\
            "Hinzufuegen von Vokabeln: <hinz> <wortDeutsch> <wortEnglisch>\n" \
            "Starten des Trainings: <train> <anzahlDerFragen>\n"\
            "Ausgabe aller Vokabeln im Vokabelheft: <vok>\n"\
            "Loeschen eines Vokabel"\
            "paars aus dem Vokabelheft: <entf> <wortDeutsch>\n"\
            "Beenden des VokabelTrainers: <q>\n"\
            "Ausgabe des Spielstands: <aus> \n"\
            "Zuruecksetzen des Spielstands: <zrk>\n"
        
    @property
    def dict(self):
        return self._dict
    
    @dict.setter
    def dict(self, var):
        """
        Setter funktioniert nur wenn der Parameter ein Dictionary ist
        """
        if(type(var) == type(dict())):
            self._dict = var
        
    def speichere_vokabeln(self):
        """
        Speichert die Vokabeln in der Datei dict.pickle
        """
        with open("dict.pickle", "wb") as f:
            pickle.dump(self.dict, f)

    def lade_vokabeln(self):
        """
        Diese Methode ladet die Vokabeln aus der Datei dict.pickle. Wenn diese dict.pickle nicht existiert
        wird eine Standardliste an Vokabeln geladen.
        """
        try:
            with open("dict.pickle", "rb") as f:
                self.dict = pickle.load(f)
        except FileNotFoundError or EOFError:
            self.dict = {"apfel": "apple", "orange": "orange", "brot": "bread"}
            
    def zufalls_schluessel(self):
        """
        gibt Generator für Zufallschlüssel zurück
        """
        k_list = list(self.dict.keys())
        random.shuffle(k_list)
        for k in k_list[:len(self.dict)]:
            yield k

    # pylint: disable=R0201
    def schliessen(self):
        """
        Schliesst das Programm
        """
        self.textEdit.clear()
        self.textEdit.append("**schliesse Vokabeltrainer**")
        sys.exit(0)

    def hinzufuegen(self, key, value):
        """
        fügt einen Wert value für einen Schlüssel key in das Wörterbuch
        dict hinzu
        """
        try:
            self.textEdit.clear()
            self.dict[key] = value
            self.textEdit.append(key + " : " + value +" hinzugefügt")
        except IndexError:
            self.textEdit.append("Bitte geben Sie ein, durch ein Leerzeichen getrenntes, Vokabelpaar an")
        self.speichere_vokabeln()

    def train_gui(self, anzahl_an_fragen):
        try:
            anzahl_an_fragen = int(anzahl_an_fragen)
        except KeyError or ValueError:
            self.textEdit.append("Bitte geben sie eine Trainingsanzahl an")
            return
        self.answers_left = anzahl_an_fragen
        self.train_flag = True
        self.train_output()

    def train_output(self):
        self.zufalls_schluessel = self.get_rand_key()
        self.textEdit.clear()
        self.textEdit.append("Übersetzen Sie: " + self.zufalls_schluessel)

    def train_user_input(self, userinput):
        lowered_input_phrase = userinput.lower()
        try:
            if self.dict[self.zufalls_schluessel] == lowered_input_phrase:
                self.richtig += 1
                self.textEdit.append("You are correct!")
            else:
                self.falsch += 1
                self.textEdit.append("That is incorrect!")
        except KeyError:
            self.textEdit.append("That is incorrect!")
            self.falsch += 1
        self.update_score_labels()
        self.answers_left -= 1
        if self.answers_left < 1:
            self.train_flag = False
            self.textEdit.clear()
            self.textEdit.append("Training beendet!")
            self.ergebnis_ausgabe()
        else:
            self.train_output()

    def update_score_labels(self):
        self.label_right.setText(str(self.richtig))
        self.label_wrong.setText(str(self.falsch))

    def get_rand_key(self):
        k_list = list(self.dict.keys())
        return k_list[random.randint(0, len(k_list)-1)]

    def trainieren(self, anzahl_an_fragen):
        """
        Fragt nach Übersetzungen für englische Wörter bis "anzahl_an_fragen"
        -oft gefragt wurde. Zaehlt die Anzahl von richtig und falsch
        beantworteten Fragen in richtig und falsch.
        KeyError wird ausgelöst wenn ein Schlüssel für self.dict
        verwendet wurde, der nicht existiert.
        TypError wird ausgelöst wenn ein Parameter kein integer war
        """
        try:
            zufalls_schluessel_generator = self.zufalls_schluessel()
            for i in range(0, int(anzahl_an_fragen)):
                if((i) % len(self.dict) == 0): # wenn Generator erschöpft
                    zufalls_schluessel_generator = self.zufalls_schluessel() # erschaffe einen neuen
                zufalls_schluessel = next(zufalls_schluessel_generator)
                lowered_input_phrase = (
                    input("Please translate:" + zufalls_schluessel + "\n")).lower()
                try:
                    if self.dict[zufalls_schluessel] == lowered_input_phrase:
                        self.richtig += 1
                        self.textEdit.append("You are correct!")
                    else:
                        self.falsch += 1
                        self.textEdit.append("That is incorrect!")
                except KeyError:
                    self.textEdit.append("That is incorrect!")
                    self.falsch += 1
        except KeyError or ValueError:
            self.textEdit.append("Bitte geben sie eine Trainingsanzahl an")
        self.ergebnis_ausgabe()

    def zuruecksetzen(self):
        """
        Startet einen neue Trainingseinheit indem es den
        Spielstand des Nutzer löscht
        """
        self.richtig = 0
        self.falsch = 0
        self.update_score_labels()

    def ergebnis_ausgabe(self):
        """
        Die Methode ergebnis_ausgabe gibt die Anzahl der richtigen und falschen
        Antworten bezogen auf die aktuelle Trainingseinheit aus.
        """
        ergebnis = "Richtige Antworten: " + str(self.richtig) + \
            " "  "Falsche Antworten: " + str(self.falsch)
        self.textEdit.append(ergebnis)

    # pylint: disable=R0201
    def gebe_befehle_aus(self):
        """
        Gibt die möglichen Befehl auf der Shell aus
        """
        self.textEdit.clear()
        self.textEdit.append(self.befehle)

    def loesche_wort(self, deutsches_wort):
        """
        loescht ein Wort aus dem Vokabelbuch
        """
        try:
            self.textEdit.clear()
            self.dict.pop(deutsches_wort)
            self.textEdit.append("Loeschen erfolgreich!")
            self.speichere_vokabeln()
        except KeyError:
            self.textEdit.append("Deutsches Wort nicht gefunden")

    def gui_command(self, command_param):
        if self.train_flag: # if user is training
            self.train_user_input(command_param)
        else:
            try:
                menue_eingabe = command_param.lower()
                self.textEdit.clear()
                command = self.commands[menue_eingabe.split(" ")[0]]
                command_arg_length = len(inspect.getargspec(command).args)
                if command_arg_length == 1 and len(menue_eingabe.split(" ")) == 1:
                    command()
                elif command_arg_length == 2 and len(menue_eingabe.split(" ")) == 2:
                    command(menue_eingabe.split(" ")[1])
                elif command_arg_length == 3 and len(menue_eingabe.split(" ")) == 3:
                    command(menue_eingabe.split(" ")[1], menue_eingabe.split(" ")[2])
                else:
                    self.textEdit.append("Befehl nicht gefunden")
            except KeyError or TypeError:
                self.textEdit.append("Befehl nicht gefunden")

    def command_loop(self):
        """
        Dauerschleife für die Menüeingabe auf der Shell
        """
        while True:
            try:
                menue_eingabe = input(
                    "Was moechten Sie tun? Geben sie <hilfe> für eine " +
                    "Befehlsausgabe ein.\n").lower()
                command = self.commands[menue_eingabe.split(" ")[0]]
                command_arg_length = len(inspect.getargspec(command).args)
                if command_arg_length == 1 and len(menue_eingabe.split(" ")) == 1:
                    command()
                elif command_arg_length == 2 and len(menue_eingabe.split(" ")) == 2:
                    command(menue_eingabe.split(" ")[1])
                elif command_arg_length == 3 and len(menue_eingabe.split(" ")) == 3:
                    command(menue_eingabe.split(" ")[1], menue_eingabe.split(" ")[2])
                else:
                    print("Befehl nicht gefunden")
            except KeyError or TypeError:
                print("Befehl nicht gefunden")

    def gebe_vokabeln_aus(self):
        """
        Gibt alle Vokabeln im Vokabelbuch auf der Konsole aus
        """
        self.textEdit.clear()
        for key, value in self.dict.items():
            self.textEdit.append(key + " " + value)
