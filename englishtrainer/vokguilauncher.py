"""
GUI Modul für den Vokabeltrainer
@author David Yesil
"""

from PyQt4 import QtCore, QtGui, uic
import sys
from vokabeltrainer import VokabelTrainer


class VokDemo(QtGui.QMainWindow):

    def __init__(self):
        super().__init__()
        self.ui = uic.loadUi('vok.ui')
        self.textEdit = self.ui.textEdit
        self.lineEdit = self.ui.lineEdit
        self.vok = VokabelTrainer(self.textEdit, self.ui.label_right, self.ui.label_wrong)
        self.ui.actionExit.setShortcut("Ctrl+q")
        self.ui.actionSave_Vocabulary.setShortcut("Ctrl+s")
        self.ui.actionExit.setStatusTip("Exit Application")
        self.connect(
            self.ui.actionExit, QtCore.SIGNAL("triggered()"), self.vok.schliessen)
        self.connect(
            self.ui.actionSave_Vocabulary, QtCore.SIGNAL("triggered()"), self.save_voc_man)
        self.connect(
            self.ui.showvocabbtn, QtCore.SIGNAL("clicked()"), self.vok.gebe_befehle_aus)
        self.connect(self.lineEdit, QtCore.SIGNAL("returnPressed ()"), self.handle_command_input)
        self.ui.show()

    def save_voc_man(self):
        self.vok.speichere_vokabeln()
        self.textEdit.clear()
        self.textEdit.append("Vokabeln gespeichert")

    def handle_command_input(self):
        self.vok.gui_command(self.lineEdit.text())
        self.lineEdit.clear()

if __name__ == "__main__":
    sys.path.append("widgets")
    app = QtGui.QApplication(sys.argv)
    vok_demo = VokDemo()
    sys.exit(app.exec_())