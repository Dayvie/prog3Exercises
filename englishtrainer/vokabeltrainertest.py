"""
Testet die Vokabeltrainer Klasse
"""
# pylint: disable=R0904

import unittest

from vokabeltrainer import VokabelTrainer


class TestVokabelTrainer(unittest.TestCase):
    """
    Testes die Vokabeltrainer Klasse
    """
    def setUp(self):
        self.vok = VokabelTrainer()

    def test_hinzufuegen(self):
        """
        Testen der Hinzufuegen-Methode
        """
        self.assertEqual(
            self.vok.dict,
            {"apfel": "apple", "orange": "orange", "brot": "bread"})
        self.vok.hinzufuegen("hund", "dog")
        self.assertEqual(
            self.vok.dict,
            {"apfel": "apple", "orange": "orange",
             "brot": "bread", "hund": "dog"})

    def test_loesche_wort(self):
        """
        Testen der LoescheWort-methode
        """
        self.assertEqual(
            self.vok.dict,
            {"apfel": "apple", "orange": "orange", "brot": "bread"})
        self.vok.loesche_wort("apfel")
        self.assertEqual(
            self.vok.dict,
            {"orange": "orange", "brot": "bread"})

    def test_trainieren_error(self):
        """
        Testen ob die Exception bei falschem Parameter ausgelöst wird
        """
        self.assertRaises(Exception, lambda: self.vok.trainieren("hund"))
