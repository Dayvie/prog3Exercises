# -*- coding: utf-8 -*-
"""
Created on Mon Oct 19 20:36:44 2015

@author: David
"""

import sys
from cx_Freeze import setup, Executable

# Dependencies are automatically detected, but it might need fine tuning.
build_exe_options = {"packages": ["os"], "excludes": ["tkinter"]}

# GUI applications require a different base on Windows (the default is for a
# console application).
base = None
if sys.platform == "win32":
    base = "Console"

setup(
    name = "Vokalbeltrainer",
    version = "0.1",
    description = "My GUI application!",
    options = {"build_exe": build_exe_options},
    executables = [Executable("Vokalbeltrainer.py", base=base)])
