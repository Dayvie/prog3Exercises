my_package package
==================

Submodules
----------

my_package.generatoren module
-----------------------------

.. automodule:: my_package.generatoren
    :members:
    :undoc-members:
    :show-inheritance:

my_package.manipulatoren module
-------------------------------

.. automodule:: my_package.manipulatoren
    :members:
    :undoc-members:
    :show-inheritance:

my_package.verdopplung module
-----------------------------

.. automodule:: my_package.verdopplung
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: my_package
    :members:
    :undoc-members:
    :show-inheritance:
