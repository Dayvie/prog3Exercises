# -*- coding: utf-8 -*-
"""
Created on Sat Oct 17 13:22:47 2015

@author: David
"""

from package_example.my_package.manipulatoren import ZahlenManipulator

z = ZahlenManipulator()
z.set_wert(5)
z.addieren(5)
print(z.get_wert())
z.dividieren(3)
print(z.get_wert())
