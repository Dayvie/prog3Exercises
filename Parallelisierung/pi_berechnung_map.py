import multiprocessing
import random, math


def monte_carlo_map(workload):
    result = 0
    for _ in range(0, workload):
        xgen = random.uniform(-1, 1)
        ygen = random.uniform(-1, 1)
        if xgen**2 + ygen**2 <= 1.0:
            result += 1
    return result


def calc_pi(l, workload, n):
    hits = 0
    for i in l:
        hits += i
    return ((hits / n)*4)/workload

if __name__ == "__main__":
    nrOfCores = multiprocessing.cpu_count()
    print('nrOfCores:', nrOfCores)
    process_pool = multiprocessing.Pool(processes=nrOfCores)
    n = 10000
    workload = 10000
    l = []
    for _ in range(0, n):
        l.append(workload)
    l2 = process_pool.map(monte_carlo_map, l)
    print(calc_pi(l2, workload, n))
