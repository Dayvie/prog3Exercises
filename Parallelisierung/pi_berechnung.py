import multiprocessing
import numpy as np


def monte_carlo_worker(q_in, q_out):
    while True:
        result = 0
        workload = q_in.get()
        for _ in range(0, workload):
            xgen = np.array(np.random.random(), np.float64)
            ygen = np.array(np.random.random(), np.float64)
            if xgen**2 + ygen**2 <= 1.0:
                result += 1
        q_out.put(result)
        q_in.task_done()


def calc_pi(q_in, workload, n):
    hits = np.array(0, np.float64)
    workload = np.array(workload, np.float64)
    n = np.array(n, np.float64)
    while not q_in.empty():
        hits += q_in.get()
    result = np.array(((hits / n)*4)/workload, np.float64)
    return result

if __name__ == "__main__":
    in_queue = multiprocessing.JoinableQueue()
    out_queue = multiprocessing.Queue()
    nrOfProcesses = multiprocessing.cpu_count()
    print(str(nrOfProcesses) + " cores")
    processes = [multiprocessing.Process(target=monte_carlo_worker, args=(in_queue, out_queue))
                 for i in range(nrOfProcesses)]
    for p in processes:
        p.start()
    n = 10000
    workload = 1000
    for i in range(0, n):
        in_queue.put(workload)
    in_queue.join()
    print("synchronized: hits and loops calculated\ncalculating pi..")
    print(calc_pi(out_queue, workload, n))
    for p in processes:
        p.terminate()
