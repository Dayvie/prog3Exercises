from PyQt4 import QtGui, uic
import random
import collections


class GalaxyDemo(QtGui.QMainWindow):
    # constructor
    def __init__(self):
        QtGui.QMainWindow.__init__(self)
        self.ui = uic.loadUi('galaxy.ui')
        widget = self.ui.galaxyWidget
        Point3D = collections.namedtuple('Point3D', ['x', 'y', 'z'])
        widget.light_position = Point3D(1e24, 1e23, 1e23)
        widget.camera_position = Point3D(1e22, 1e22, 1e22)
        widget.show()
        Sphere3D = collections.namedtuple('Mass3D', ['x', 'y', 'z', 'r'])
        sphere_list = []
        c_max = max(widget.camera_position) * 0.5
        def rand_coord(m):
            return (1 - 2 * random.random()) * m
        for r in range(1, 100):
            m = Sphere3D(
                rand_coord(c_max), rand_coord(c_max), rand_coord(c_max),
                c_max * .1 * random.random())
            sphere_list.append(m)
        widget.render(sphere_list)
        self.ui.show()

if __name__ == '__main__':
    import sys
    import os
    sys.path.append("widgets")
    env = os.environ.copy()
    env['PYQTDESIGNERPATH'] = 'python'
    app = QtGui.QApplication(sys.argv)
    galaxyDemo = GalaxyDemo()
    sys.exit(app.exec_())
