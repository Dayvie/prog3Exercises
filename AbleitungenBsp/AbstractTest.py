"""
Testing Abstract classes
"""

from abc import ABCMeta, abstractmethod


class CelestialBody(metaclass=ABCMeta):

    __metaclass__ = ABCMeta

    @abstractmethod
    def kek(self):
        print("kek my sides")


if __name__ == "__main__":
    cel = CelestialBody()
    cel.kek()